package petr_bogomolov.com.animal.objects.land;

import petr_bogomolov.com.animal.objects.Animals;
import petr_bogomolov.com.gender.Gender;
import petr_bogomolov.com.interfaces.Breathe;
import petr_bogomolov.com.interfaces.Move;

public class Wolf extends Animals implements Move, Breathe {


    public Wolf(String name, Gender gender, int age, int lifespan) {
        super(name, gender, age, lifespan);
    }


    @Override
    public void move() {
        System.out.println("I can breath on land");
    }

    @Override
    public void breathe() {
        System.out.println("I can run");
    }
}
