package petr_bogomolov.com.animal.objects;

import petr_bogomolov.com.gender.Gender;
import petr_bogomolov.com.interfaces.GrowUp;
import petr_bogomolov.com.interfaces.Present;

public class Animals implements Present, GrowUp {

    private String name;
    private final Gender gender;
    private int age;
    private final int lifespan;

    public Animals(String name, Gender gender, int age, int lifespan) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.lifespan = lifespan;
    }

    public String getName() {
        return name;
    }


    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }


    public int getLifespan() {
        return lifespan;
    }

    public void growUp() {
        if (getAge() < 0) {
            System.out.println("You enter not corect age");
        } else if (getAge() > getLifespan()) {
            System.out.println("I die");
        } else if (getAge() > getLifespan() / 2) {
            System.out.println("I'm very old " + getName());
        } else if (getAge() > getLifespan() / 3 && getAge() <= getLifespan() / 2) {
            System.out.println("i'm mature " + getName());
        } else if (getAge() <= getLifespan() / 3) {
            System.out.println("i am young " + getName());
        }

    }

    @Override
    public void present() {
        System.out.println("Hi,i'm " + getName() + " i'm " + getGender() + " and i'm " + getAge() +
                " years old and i'm going to live " + getLifespan() + " years");
    }
}
