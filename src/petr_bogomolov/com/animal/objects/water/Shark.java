package petr_bogomolov.com.animal.objects.water;

import petr_bogomolov.com.animal.objects.Animals;
import petr_bogomolov.com.gender.Gender;
import petr_bogomolov.com.interfaces.Breathe;
import petr_bogomolov.com.interfaces.Move;

public class Shark extends Animals implements Move, Breathe {


    public Shark(String name, Gender gender, int age, int lifespan) {
        super(name, gender, age, lifespan);
    }

    @Override
    public void breathe() {
        System.out.println("i can breathe in water");
    }

    @Override
    public void move() {
        System.out.println("I can swim");
    }
}
