package petr_bogomolov.com;

import petr_bogomolov.com.animal.objects.air.Eagle;
import petr_bogomolov.com.animal.objects.water.Shark;
import petr_bogomolov.com.animal.objects.land.Wolf;
import petr_bogomolov.com.gender.Gender;

public class AnimalMain {

    public static void main(String[] args) {

        Wolf wolf = new Wolf("wolf Bolto", Gender.MAN, 6, 18);
        Shark shark = new Shark("shark Nemo",Gender.WOMEN, 24,40);
        Eagle eagle = new Eagle("eagle Habib",Gender.MAN, 3, 12);

        wolf.present();
        wolf.move();
        wolf.breathe();
        wolf.growUp();

        System.out.println();

        shark.present();
        shark.move();
        shark.breathe();
        shark.growUp();

        System.out.println();

        eagle.present();
        eagle.move();
        eagle.breathe();
        eagle.growUp();

    }
}
